					<section id="primary">
						<div id="primary-inner">

							<section class="forgot-change form">
								
							<h2>Change your password</h2>
							
							<form accept-charset="UTF-8" action="/users/password" class="new_user" id="new_user" method="post">
								
								<div style="margin:0;padding:0;display:inline">
									<input name="utf8" value="✓" type="hidden"><input name="_method" value="put" type="hidden"><input name="authenticity_token" value="nE+jKSB62kI9icivRcZoCVVXv7iMxyqI+ROD8RY3AXY=" type="hidden">
								</div><input id="user_reset_password_token" name="user[reset_password_token]" value="JDiLDy7FkQn78CcXk3fT" type="hidden">
								
								<fieldset class="inputs">
									<ol>
										<li>											
											<label for="user_password">New password</label>
											<input autofocus="autofocus" id="user_password" name="user[password]" size="30" type="password">
										</li>
										<li>	
											<label for="user_password_confirmation">Confirm password</label>
											<input id="user_password_confirmation" name="user[password_confirmation]" size="30" type="password">
										</li>
									</ol>
								</fieldset>

								<fieldset class="actions">
									<ol>
										<li><input name="commit" value="Change my password" type="submit"></li>
									</ol>
								</fieldset>
							</form>

							</section>

						</div>
					</section>

<?php require_once('sidebar.php'); ?>