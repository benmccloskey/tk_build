<!DOCTYPE html>
<html lang="en">

	<head>

		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width; initial-scale = 1.0; maximum-scale=1.0; user-scalable=no" />
		<link rel="stylesheet" type="text/css" href="//cloud.typography.com/6165652/789662/css/fonts.css" />
		<link rel="stylesheet" type="text/css" href="/build/css/reset.css" />
		<link rel="stylesheet" type="text/css" href="/build/css/screen.css" />
		<script src="/build/js/jquery-1.10.2.min.js"></script>
		<script src="/build/js/jquery-ui-1.10.4.min.js"></script>
		<script src="/build/js/jquery.autosize.min.js"></script>
		<script src="/build/js/page.js"></script>

	</head>

	<body>

		<div id="wrapper">

			<header>
				
				<div class="inner">
				
					<div id="logo"><a href="/build/?p=wall">Tenant King</a></div>

					<?php if (isset($_GET['p']) && $_GET['p'] != 'login' && $_GET['p'] != 'activate') { //only print nav if this is neither the front page nor the login page ?>

					<nav id="main-nav"><ul>
						<li class="home<?php if($_GET['p'] == 'wall'){ print ' active'; } ?>"><a href="/build/?p=wall">Home</a></li>
						<li class="topics<?php if($_GET['p'] == 'topics'){ print ' active'; } ?>"><a href="/build/?p=topics">General Talk</a></li>
						<li class="classifieds<?php if($_GET['p'] == 'classifieds'){ print ' active'; } ?>"><a href="/build/?p=classifieds">Classifieds</a></li>
						<li class="settings<?php if($_GET['p'] == 'settings'){ print ' active'; } ?>"><a href="/build/?p=settings">Settings</a></li>
						<li class="messages<?php if($_GET['p'] == 'messages'){ print ' active'; } ?>"><a href="/build/?p=messages">Messages</a> <span class="nav-alert">4</span></li>
					</ul></nav>

					<?php } elseif (!isset($_GET['p']) || $_GET['p'] == 'activate') { // print login button only if this is front page ?>

					<div id="header-login"><a href="/build?p=login">Log In</a></div>

					<?php } ?>
				
				</div>
			
			</header>
			
			<?php if($_GET['p'] == 'settings'): ?>
			
			<div id="notifications">
				<div class="inner">
					<button id="notifications-close">Close</button>
					<p>Invalid last name. You are not the boss.</p>
				</div>
			</div>

			<?php endif; ?>

			<?php if(isset($_GET['success'])){ 

				print '
			<div id="notifications">
				<div class="inner">
					<button id="notifications-close">Close</button>';

				switch ((isset($_GET['success']) ? $_GET['success'] : '')) {
					case 'cc':
						print '<p class="success">Address verification successful.</p>';
						break;

					case 'mail':
						print '<p class="success">Sign up complete. You will receive the postcard in 5–7 days.</p>';
						break;
					
					case 'bill':
						print '<p class="success">Sign up complete. We will send you an activation e-mail once the utility bill is reviewed.</p>';
						break;
				}
				print '
				</div>
			</div>';


			} ?>

			<section id="content"<?php if(!isset($_GET['p'])){ print ' class="front"'; } ?>>

				<?php if(isset($_GET['p']) && $_GET['p'] !='login' && $_GET['p'] !='verify' && $_GET['p'] !='activate'){ ?>
				<div class="inner">
				<?php } ?>
