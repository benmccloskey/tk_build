			<div class="inner">
				<div id="welcome-container">
					
					<?php
					$is_login =  false; // check if this is login page
					$submit_text = 'Activate My Account';
					$welcome_title = 'Private Network For Your Building.';
					$form_links = '<ol><li>Already a member? <a href="/build?p=login">Sign in</a></li><li class="activation-link"><a href="#request-invitation">Request an Invitation</a></li></ol>';
					?>

					<h1>Activate your Tenant King Account</h1>

					<div id="welcome-main">
						
						<div id="welcome-primary">
							<form novalidate="novalidate" method="post" id="new_user" class="formtastic user" action="/users/sign_in" accept-charset="UTF-8">
								<div style="margin:0;padding:0;display:inline">
									<input type="hidden" value="✓" name="utf8"><input type="hidden" value="Wb+BxdSv1M1QpRfaSGFo1hRBRmJJ4ZZbf/HFgKSyaSM=" name="authenticity_token">
								</div>
								<fieldset class="inputs">
									<ol>
										<?php if (!$is_login): ?>
										<li>	
											<label for="invitation-code">Invitation Code</label>
											<input class="form-text validation-error" type="text" id="invitation-code" name="invitation-code" />
										</li>
										<?php endif; ?>
										<li id="user_email_input" class="email input required stringish">
											<label for="user_email" class="label">Email<abbr title="required">*</abbr></label><input type="email" value="" name="user[email]" maxlength="255" id="user_email" class="form-text">
										</li>
										<li id="user_remember_me_input" class="boolean input optional">
											<input type="hidden" value="0" name="user[remember_me]"><label for="user_remember_me" class=""><input type="checkbox" value="1" name="user[remember_me]" id="user_remember_me">Remember me</label>
										</li>
									</ol>
								</fieldset>
								<fieldset class="actions">
									<ol>
										<li id="user_submit_action" class="action input_action">
											<input type="submit" value="<?php print $submit_text; ?>" name="commit" class="form-submit">
										</li>
									</ol>
								</fieldset>
							</form>
							<div id="form-links">
								<?php print $form_links; ?>
							</div>
						</div>

						<div id="welcome-secondary">

							<?php if (!$is_login): ?>

							<ul id="welcome-front-features">
								<li id="connect-callout">Connect to neighbors in and around your building.</li>
								<li id="share-callout">Share with confidence. <br />Only verified tenants.</li>
								<li id="buy-callout">Buy and sell. <br />Simple local classifieds.</li>
							</ul>

							<?php else: ?>

							<ul id="welcome-login-features">
								<li id="limit-callout">You can limit the audience of your post.</li>
							</ul>

							<aside>When publishing a post, you can choose who you share with. Make your posts visible to tenants in your building, management company or the whole neighborhood. <strong>It’s up to you!</strong></aside>

							<?php endif; ?>

						</div>

						<div class="clear">&nbsp;</div>

					</div>

					<div id="welcome-mobile-only">

						<div id="welcome-promo-1">
							<h3>We’re Launching In Long Island City</h3>
							<ul id="welcome-stats">
								<li id="welcome-stats-buildings"><em>16</em> Buildings</li>
								<li id="welcome-stats-units"><em>5,000</em> Units</li>
								<li id="welcome-stats-buyers"><em>9,000</em> Neighbors</li>
							</ul>
						</div>

						<div id="welcome-promo-2">
							
							<div class="welcome-list-link">
								<a href="#">The list of Pilot Buildings</a>
								<ul id="welcome-building-list">
									<li>4720 Center Blvd</li>
									<li>4630 Center Blvd</li>
									<li>4540 Center Blvd</li>
									<li>4615 Center Blvd</li>
									<li>4545 Center Blvd</li>
									<li>Avalon Riverview North</li>
									<li>Avalon Riverview</li>
									<li>4-83 48th Ave</li>
									<li>4-89 48th Ave</li>
									<li>4-95 48th Ave</li>
									<li>Citylights</li>
									<li>4705 Center Blvd</li>
									<li>Linc LIC</li>
									<li>5SL</li>
									<li>The Gantry</li>
									<li>Gantry Park Landing</li>
								</ul>
							</div>

							<div id="welcome-do-container">
								<h4>What can I do here?</h4>
								<ul id="welcome-do-list">
									<li id="welcome-do-petitions">
										<h4>Connect with the people in your building and your neighborhood.</h4>
										<p>If it's impacting you, it's impacting your neighbors as well. From restaurant recommendations to management complaints, there is no substitute for local knowledge from real people.</p>
									</li>
									<li id="welcome-do-community">
										<h4>No craigslist randoms, no wait times.</h4>
										<p>Looking to make room for that new couch? Offer your old one to your neighbors. No waiting for management approval, no wondering who is really on the way over.</p>
									</li>
									<li id="welcome-do-verified">
										<h4>Only verified tenants.</h4>
										<p>Every member has been verified via traditional mail. No stalkers, no marketers, no bots, no management companies. Just you and your fellow tenants.</p>
									</li>
								</ul>

								<div id="welcome-join-button"><a href="#">Click Here to Join</a></div>
							
							</div>

						</div>

					</div>

					<div class="clear">&nbsp;</div>

				</div>
			</div>

			<div id="welcome-desktop-only">

					<h3 id="welcome-desktop-promo-1-h3">So what can I do here?</h3>

					<div id="welcome-desktop-promo-1">
						
						<ol>
							<li id="welcome-desktop-promo-connect">
								<h4>Connect with the people in your building and your neighborhood.</h4>
								<p>If it's impacting you, it's impacting your neighbors as well. From restaurant recommendations to management complaints, there is no substitute for local knowledge from real people.</p>
							</li>
							<li id="welcome-desktop-promo-classifieds">
								<h4>No craigslist randoms, no wait times.</h4>
								<p>Looking to make room for that new couch? Offer your old one to your neighbors. No waiting for management approval, no wondering who is really on the way over.</p>
							</li>
							<li id="welcome-desktop-promo-verified">
								<h4>Only verified tenants.</h4>
								<p>Every member has been verified via traditional mail. No stalkers, no marketers, no bots, no management companies. Just you and your fellow tenants.</p>
							</li>
						</ol>
						<div class="clear">&nbsp;</div>
					</div>

					<div id="welcome-desktop-promo-2">
						<h3>We’re Launching In Long Island City</h3>
						<ul id="welcome-stats">
							<li id="welcome-stats-buildings"><em>16</em> Buildings</li>
							<li id="welcome-stats-units"><em>5,000</em> Units</li>
							<li id="welcome-stats-buyers"><em>9,000</em> Neighbors</li>
						</ul>
					</div>

					<div id="welcome-desktop-request-invitation">

						<h3>Request an Invitation</h3>
						
						<form id="request-invitation">
							<fieldset class="inputs">
								<ol>
									<li id="request-invitation-building-address">
										<label for="building-address">Building Address *</label>
										<input type="text" id="building-address" name="building-address">
									</li>
									<li id="request-invitation-zip-code">
										<label for="zip-code">ZIP Code *</label>
										<input type="text" id="zip-code" name="zip-code">
									</li>
									<li id="request-invitation-email-address">
										<label for="email-address">Email Address *</label>
										<input type="text" id="email-address" name="email-address">
									</li>
								</ol>
							</fieldset>
							<fieldset class="actions">
								<ol>
									<li id="request_submit_action" class="action input_action">
										<input type="submit" value="Submit" name="request" class="form-submit">
									</li>
								</ol>
							</fieldset>
						</form>
						<div class="clear">&nbsp;</div>
						<p>Tenant King is originally launched in the following Long Island City residential buildings: 4720 Center Blvd, 4630 Center Blvd, 4540 Center Blvd, 4615 Center Blvd, 4545 Center Blvd, Avalon Riverview North, Avalon Riverview, Citylights, 4705 Center Blvd, Linc LIC, 5SL, The Gantry and Gantry Park Landing. We are constantly growing, so register with your information and you will be the first to get an invitation, when we add your building to the Tenant King family.</p>

					</div>

				</div>










