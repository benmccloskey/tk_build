<?php if($_GET['p'] == 'classifieds'){
	$title = 'Classifieds';
}else{
	$title = "General Talk";
}?>
					<section id="primary">
						<div id="primary-inner">

							<h1 id="page-title" class="page-title-category"><?php print $title; ?> <a class="show-post-form delete-link add-post-link" href="#">+ Add Post</a></h1>

							<?php if($_GET['p'] == 'classifieds'){ ?>

							<table id="topics-table" class="classifieds">
								<thead>
									<tr>
										<th class="title-col" colspan="2">Topic</th>
									</tr>
								</thead>
								<tbody>
									<tr class="odd">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb1.png" alt=""><a href="#">Renting a room in my apartment</a> <span><strong>$123</strong> &bull; Added by Michael R.</span></div></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">2:44PM</div>
											<div class="topics-post-comments">12</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb2.png" alt=""><a href="#">Removal of Existing Pressurized Wall</a> <span><strong>$58</strong> &bull; Added by Pamela H.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb3.png" alt=""><a href="#">Deposit Reduction</a> <span><strong>$50 for all</strong> &bull; Added by Grag G.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb1.png" alt=""><a href="#">Late fee illegal even if original lease...</a> <span><strong>SOLD</strong> &bull; Added by Patricia M.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb2.png" alt=""><a href="#">Rights as a non-leasholding roommate?</a> <span><strong>$785</strong> &bull; Added by David D.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb3.png" alt=""><a href="#">Signing new lease, have a big issue</a> <span><strong>$8 each</strong> &bull; Added by Mark H.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb1.png" alt=""><a href="#">Unreasonable refusal to sublease</a> <span><strong>$25</strong> &bull; Added by John M.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb2.png" alt=""><a href="#">Replacing Myself in Three Bedroom</a> <span><strong>$150 OBO</strong> &bull; Added by Zach W.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb3.png" alt=""><a href="#">Anyone seen the Gantry Park Landing?</a> <span><strong>$45</strong> &bull; Added by Bryan E.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb1.png" alt=""><a href="#">Classic switch and bait...</a> <span><strong>$30</strong> &bull; Added by Michael R.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb2.png" alt=""><a href="#">The management hates me</a> <span><strong>$80</strong> &bull; Added by Michael R.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><img src="/build/images/thumbs/thumb3.png" alt=""><a href="#">Let’s gang up on the doorman tonight</a> <span><strong>$43 or best offer</strong> &bull; Added by Michael R.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
								</tbody>
							</table>

							<?php }else{ ?>

							<table id="topics-table">
								<thead>
									<tr>
										<th class="title-col" colspan="2">Topic</th>
									</tr>
								</thead>
								<tbody>
									<tr class="odd">
										<td class="title-col"><div><a href="#">Renting a room in my apartment</a> <span>Added by Michael R.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><a href="#">Removal of Existing Pressurized Wall</a> <span>Added by Pamela H.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><a href="#">Deposit Reduction</a> <span>Added by Grag G.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><a href="#">Late fee illegal even if original lease...</a> <span>Added by Patricia M.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><a href="#">Rights as a non-leasholding roommate?</a> <span>Added by David D.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><a href="#">Signing new lease, have a big issue</a> <span>Added by Mark H.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><a href="#">Unreasonable refusal to sublease</a> <span>Added by John M.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><a href="#">Replacing Myself in Three Bedroom</a> <span>Added by Zach W.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><a href="#">Anyone seen the Gantry Park Landing?</a> <span>Added by Bryan E.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><a href="#">Classic switch and bait...</a> <span>Added by Michael R.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="odd">
										<td class="title-col"><div><a href="#">The management hates me</a> <span>Added by Michael R.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
									<tr class="even">
										<td class="title-col"><div><a href="#">Let’s gang up on the doorman tonight</a> <span>Added by Michael R.</span></div></td>
										<td class="postdate-col">
											<div class="topics-post-date">Oct 29</div>
											<div class="topics-post-comments">4</div>
										</td>
									</tr>
								</tbody>
							</table>

							<?php } ?>

							<div id="pager">
								<a class="pager-add-post-link" href="#">+ Add Post</a>
								1-20 of 365
								<ul>
									<li id="pager-previous"><a href="#">Previous</a></li>
									<li id="pager-next"><a href="#">Next</a></li>
								</ul>
							</div>
						</div>
					</section>

<?php require_once('sidebar.php'); ?>