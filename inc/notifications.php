					<section id="primary">
						<div id="primary-inner">
							<div class="settings-form">
								
								<h2>E-mail Notification Settings</h2>
								<form novalidate="novalidate" method="post" id="edit_user_3" enctype="multipart/form-data" class="notifications formtastic user" action="/settings" accept-charset="UTF-8">
									<div style="margin:0;padding:0;display:inline">
										<input type="hidden" value="✓" name="utf8">
										<input type="hidden" value="put" name="_method">
										<input type="hidden" value="gLBaNn1gSlKODWrhqeosozYQ3SWB6VozDZUPYDoVX7Q=" name="authenticity_token">
									</div>
									<fieldset class="inputs">
										<ol>
											<li id="user_notifications_my_posts" class="radio input">
												<label class=" label">My Posts</label>
												<div class="radios-container">
													<ul class="radios-control">
														<li for="my_posts_on" class="on"><button class="on">On</button></li>
														<li class="radios-control-last" for="my_posts_off"><button>Off</button></li>
													</ul>
													<div class="radios-input">
														<select name="my_posts">
															<option value="On">On</option>
															<option value="Off">Off</option>
														</select>
													</div>
													<label>When someone comments on my post</label>
												</div>
											</li>
											<li id="user_notifications_my_comments" class="radio input">
												<label class=" label">My Comments</label>
												<div class="radios-container">
													<ul class="radios-control">
														<li for="other_comments_on" class="on"><button class="on">On</button></li>
														<li class="radios-control-last" for="other_comments_off"><button>Off</button></li>
													</ul>
													<div class="radios-input">
														<select name="my_comments">
															<option value="On">On</option>
															<option value="Off">Off</option>
														</select>
													</div>
													<label>When someone comments on a post I commented on</label>
												</div>
											</li>
											<li id="user_notifications_direct_messages" class="radio input">
												<label class=" label">Direct Messages</label>
												<div class="radios-container">
													<ul class="radios-control">
														<li for="post_inquiry_on" class="on"><button class="on">On</button></li>
														<li class="radios-control-last" for="post_inquiry_off"><button>Off</button></li>
													</ul>
													<div class="radios-input">
														<select name="direct_messages">
															<option value="On">On</option>
															<option value="Off">Off</option>
														</select>
													</div>
													<label>When someone contacts me directly about a post</label>
												</div>
											</li>
											<li id="user_notifications_new_posts_city" class="radio multiple input">
												<label class=" label">New Posts <span>(My Area)</span></label>
												<div class="radios-container">
													<ul class="radios-control">
														<li for="new_posts_city_instant"><button>Instant</button></li>
														<li for="new_posts_city_daily" class="on"><button class="on">Daily</button></li>
														<li for="new_posts_city_weekly"><button>Weekly</button></li>
														<li class="radios-control-last" for="new_posts_city_never"><button>Never</button></li>
													</ul>
													<div class="radios-input">
														<select name="new_posts_city">
															<option value="Instant">Instant</option>
															<option value="Daily">Daily</option>
															<option value="Weekly">Weekly</option>
															<option value="Never">Never</option>
														</select>
													</div>
													<label>New posts in the Long Island City Circle</label>
												</div>
											</li>
											<li id="user_notifications_new_posts_building" class="radio multiple input">
												<label class=" label">New Posts <span>(My Building)</span></label>
												<div class="radios-container">
													<ul class="radios-control">
														<li for="new_posts_building_instant"><button>Instant</button></li>
														<li for="new_posts_building_daily"><button>Daily</button></li>
														<li for="new_posts_building_weekly"><button>Weekly</button></li>
														<li class="radios-control-last on" for="new_posts_building_never"><button class="on">Never</button></li>
													</ul>
													<div class="radios-input">
														<select name="new_posts_building">
															<option value="Instant">Instant</option>
															<option value="Daily">Daily</option>
															<option value="Weekly">Weekly</option>
															<option value="Never">Never</option>
														</select>
													</div>
													<label>New posts in the Gantry Park Landing Circle</label>
												</div>
											</li>
											<li id="user_notifications_new_posts_floor" class="radio multiple input">
												<label class=" label">New Posts <span>(My Floor)</span></label>
												<div class="radios-container last">
													<ul class="radios-control">
														<li for="new_posts_floor_instant" class="on"><button class="on">Instant</button></li>
														<li for="new_posts_floor_daily"><button>Daily</button></li>
														<li for="new_posts_floor_weekly"><button>Weekly</button></li>
														<li class="radios-control-last" for="new_posts_floor_never"><button>Never</button></li>
													</ul>
													<div class="radios-input">
														<select name="new_posts_floor">
															<option value="Instant">Instant</option>
															<option value="Daily">Daily</option>
															<option value="Weekly">Weekly</option>
															<option value="Never">Never</option>
														</select>
													</div>
													<label>New posts in the 4th Floor Circle</label>
												</div>
											</li>
										</ol>
									</fieldset>
									<fieldset class="actions">
										<ol>
											<li id="user_submit_action" class="action input_action ">
												<input type="submit" value="Save" name="commit">
											</li>
										</ol>
									</fieldset>
								</form>

							</div>
						</div>
					</section>

<?php require_once('sidebar.php'); ?>