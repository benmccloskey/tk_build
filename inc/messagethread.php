					<section id="primary">
						<div id="primary-inner">

							<button id="test-images">Images Switcher</button>

							<h1 id="page-title" class="page-title-message">RE: Borrow a ladder? <a class="show-original-post" href="#">Show Original Post</a></h2>
								
							<div id="message-thread-container">
								<article class="message">

									<div class="message-date">Today</div>
									<div class="message-avatar"><a href="#"><img src="/build/images/avatars/avatar_patricia.png" alt="Patricia M Avatar" /></a></div>
									<div class="message-meta"><a href="#">Patricia M</a> <span>from <a href="#">25th Floor</a> in <a href="#">Pets &amp; Maids</a></span></div>
									<div class="message-body"><p>Our family just got a baby Gremlin about two weeks ago. The kids affectionately named her Gizmo, and played with her in the rooftop pool. Has anyone seen Gizmo?</p></div>
									
								</article>

								<article class="message">

									<div class="message-date">Today</div>
									<div class="message-avatar"><a href="#"><img src="/build/images/avatars/avatar_patricia.png" alt="Patricia M Avatar" /></a></div>
									<div class="message-meta"><a href="#">Patricia M</a> <span>from <a href="#">25th Floor</a> in <a href="#">Pets &amp; Maids</a></span></div>
									<div class="message-body"><p>Our family just got a baby Gremlin about two weeks ago. The kids affectionately named her Gizmo, and played with her in the rooftop pool. Has anyone seen Gizmo?</p></div>
									
								</article>

								<article class="message">

									<div class="message-date">Today</div>
									<div class="message-avatar"><a href="#"><img src="/build/images/avatars/avatar_patricia.png" alt="Patricia M Avatar" /></a></div>
									<div class="message-meta"><a href="#">Patricia M</a> <span>from <a href="#">25th Floor</a> in <a href="#">Pets &amp; Maids</a></span></div>
									<div class="message-body"><p>Our family just got a baby Gremlin about two weeks ago. The kids affectionately named her Gizmo, and played with her in the rooftop pool. Has anyone seen Gizmo?</p></div>
									
								</article>

								<div class="message-actions"><div class="message-actions-container">
									<div class="message-actions-form-container">
										<form>
											<fieldset class="inputs">
												<ol>
													<li id="message_body_input" class="text input required with-images">
									                    <textarea rows="20" name="post[body]" id="message_body"></textarea>
														<div class="images-container">
										                    
															<div id="image_13" class="image-container">
																<a href="/build/images/test.png"><img src="/build/images/test.png" alt="Thumb_test"></a>
																<div class="name"></div>
																<div class="actions">
																	<a rel="nofollow" data-remote="true" data-method="delete" data-confirm="Are you sure?" href="/images/13">remove</a>
																</div>
															</div>

															<div id="image_14" class="image-container">
																<a href="/build/images/test.png"><img src="/build/images/test.png" alt="Thumb_test"></a>
																<div class="name"></div>
																<div class="actions">
																	<a rel="nofollow" data-remote="true" data-method="delete" data-confirm="Are you sure?" href="/images/13">remove</a>
																</div>
															</div>

										                    <div class='image-container' id='upload-container-{%=o.domId%}'>
																<div class='upload'>
																	test.png
																	<div class='progress'>
																		<div class='bar' style='width: 65%;'></div>
																	</div>
																</div>
															</div>
														</div>
														<div class="clear">&nbsp;</div>
								                	</li>											
							                	</ol>
											</fieldset>
											<fieldset class="actions">
								                <div style="margin-left: 2em">
								                    <button class="post-submit" type="submit" name="button">Submit</button>
								                    <div class="file_button_container">
								                    	<div id="fake-add-image-button">+</div>
								                        <input type="file" name="image[file]" multiple="multiple" id="new_image" data-url="/images" />
								                    </div>
								                </div>
								            </fieldset>
										</form>
									</div>
								</div></div>

							</div>

						</div>
					</section>

<?php require_once('sidebar.php'); ?>

