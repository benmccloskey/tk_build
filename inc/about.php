					<section id="primary">
						<div id="primary-inner">

							<h1 id="page-title" class="page-title-about">About Us</h1>

							<div id="static-body">

								<h2>What is Tenant King?</h2>

								<p>Tenant King connects you with the people in your building and your neighborhood. It is a private network of neighbors designed to strengthen the communities within Long Island City’s high-rises.</p>
								<p>Tenant King allows neighbors to easily buy, sell, and swap furniture, electronics, and services; It allows tenants to anonymously voice their concerns about the building’s management; it allows residents to arrange community activities, such as barbeques and parties.</p>
								<p>Tenant King is the simplest way to connect with your neighbors and strengthen the community within your building. Possible uses for Tenant King include:</p> 
								
								<ul>	
									<li>Moving and want to sell your furniture? Sell it to your neighbors</li>
									<li>Have tickets to the Yankees game but won’t be able to make it? Sell them or give them away to your neighbor next door</li>
									<li>Looking for a babysitter who you can trust? Ask for recommendation from your neighbors</li>
									<li>Receive a blessing from fellow residents to have a rooftop party</li>
									<li>Invite your neighbors to the game room for a poker night</li>
									<li>Arrange a playdate for your child with the kids downstairs</li>
								</ul>

								<h2>Who is Tenant King?</h2>
								<p>Tenant King is a group of passionate locals dedicated to connecting and strengthening the communities within Long Island City’s high-rises.</p>
							
							</div>

						</div>
					</section>

<?php require_once('sidebar.php'); ?>