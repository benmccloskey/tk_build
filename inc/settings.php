					<section id="primary">
						<div id="primary-inner">
							<div class="settings-form">
								
								<h2>General settings</h2>
								<form novalidate="novalidate" method="post" id="edit_user_3" enctype="multipart/form-data" class="formtastic user" action="/settings" accept-charset="UTF-8">
									<div style="margin:0;padding:0;display:inline">
										<input type="hidden" value="✓" name="utf8">
										<input type="hidden" value="put" name="_method">
										<input type="hidden" value="gLBaNn1gSlKODWrhqeosozYQ3SWB6VozDZUPYDoVX7Q=" name="authenticity_token">
									</div>
									<fieldset class="inputs">
										<ol>
											<li id="user_email_input" class="email input required stringish">
												<label for="user_email" class=" label">Email<abbr title="required">*</abbr></label>
												<input type="email" value="patrik@seocialmedia.com" name="user[email]" maxlength="255" id="user_email">
											</li>
											<li id="user_first_name_input" class="string input required stringish">
												<label for="user_first_name" class=" label">First name<abbr title="required">*</abbr></label>
												<input type="text" value="Admin" name="user[first_name]" maxlength="255" id="user_first_name">
											</li>
											<li id="user_last_name_input" class="string input required stringish">
												<label for="user_last_name" class=" label">Last name<abbr title="required">*</abbr></label>
												<input type="text" value="Boss" name="user[last_name]" maxlength="255" id="user_last_name" class="validation-error">
											</li>
											<li id="user_notifications" class="radio input required">
												<label class=" label">Notifications<abbr title="required">*</abbr></label>
												<div class="radios-container">
													<label>Buy &amp; Sell</label>
													<ul class="radios-control">
														<li class="radios-control-yes" for="buy_sell_yes"><button class="on">Yes</button></li>
														<li class="radios-control-no" for="buy_sell_no"><button>No</button></li>
													</ul>
													<div class="radios-input">
														<input type="radio" value="Yes" name="buy_sell" id="buy_sell_yes">
														<label for="buy_sell_yes" class="radio-label">Yes</label>
														<input type="radio" value="No" name="buy_sell" id="buy_sell_no"> No
														<label for="buy_sell_no" class="radio-label">No</label>
													</div>
												</div>
											</li>
											<li id="user_avatar_input" class="file input optional">
												<label for="user_avatar" class=" label">Avatar</label>
												<img id="avatar-thumb" src="http://tenantking.com/uploads/user/avatar/3/thumb_tumblr_mm0zwqjLBF1qfa0zmo1_500.jpg" alt="Thumb_tumblr_mm0zwqjlbf1qfa0zmo1_500">
												<div id="facebook-avatar"><a href="/users/auth/facebook">Use Facebook Profile Photo</a></div>
												<input type="file" name="user[avatar]" id="user_avatar">
												<input type="hidden" value="0" name="user[remove_avatar]">
												<label for="user_remove_avatar" class="inline-label"><input type="checkbox" value="1" name="user[remove_avatar]" id="user_remove_avatar">Remove avatar</label>
											</li>
											<li id="user_avatar_cache_input" class="hidden input optional">
												<input type="hidden" name="user[avatar_cache]" id="user_avatar_cache">
											</li>
										</ol>
									</fieldset>
									<fieldset class="actions">
										<ol>
											<li id="user_submit_action" class="action input_action ">
												<input type="submit" value="Update User" name="commit">
											</li>
										</ol>
									</fieldset>
								</form>

							</div>
						</div>
					</section>

<?php require_once('sidebar.php'); ?>