					<section id="primary">
						<div id="primary-inner">

							<section id="post-form" class="post-form-teaser">

								<h2>Add a Post <a class="show-post-form delete-link add-post-link" href="#">+ Add Post</a></h2>
								<form>
									<textarea name="message" id="message">Click to start entering a new post...</textarea>
								</form>
							</section>

							<div id="posts-container">

								<article class="post">

									<h3><a href="#">Missing Gremlin</a></h3>
									<div class="post-status-tag">
										<div class="post-date">Edit | Delete</div>
										<div class="post-status">Sold</div>
									</div>
									<div class="post-avatar"><a href="#"><img src="/build/images/avatars/avatar_patricia.png" alt="Patricia M Avatar" /></a></div>
									<div class="post-meta"><a href="#">Mike D</a> &bull; 4615 Center Blvd &bull; <span class="walking">5 Minutes</span> &bull; <a href="#">Classifieds</a></div>
									<div class="post-body"><p>Our family just got a baby Gremlin about two weeks ago. The kids affectionately named her Gizmo, and played with her in the rooftop pool. Has anyone seen Gizmo?</p></div>
									<div class="post-comments">
										<ol class="post-comments-list">
											<li>
												<a data-remote="true" class="show-more-comments" href="#">View 3 more comments</a>
											</li>
											<li class="comment">
												<div class="comment-date">Today</div>
												<div class="comment-avatar"><a href="#"><img src="/build/images/avatars/avatar_peter.png" alt="Peter G Avatar" /></a></div>
												<div class="comment-meta"><a href="#">Peter G</a> from <a href="#">2nd Floor</a></div>
												<div class="comment-body"><p>No, but our pool was swarming with hairy monsters the other day and now the poolboy is missing. Has anyone seen the poolboy?</p></div>
											</li>
										</ol>
									</div>
									<div class="post-actions"><div class="post-actions-container">
										<div class="post-actions-form-container"><form>
											<span class="comment-container">
												<textarea id="comment_body" class="comment-body" rows="1" name="comment[body]" cols="40" style="overflow: hidden; word-wrap: break-word; resize: none; height: 33px;"></textarea>
											</span>
											<button class="comment-submit">Submit</button>
										</form></div>
									</div></div>

								</article>


								<article class="post">

									<h3><a href="#">Reminder: Labor Day BBQ tomorrow!</a></h3>
									<div class="post-status-tag">
										<div class="post-date">Aug 31</div>
										<div class="post-status post-price">$1,345,789.00</div>
									</div>
									<div class="post-avatar"><a href="#"><img src="/build/images/avatars/avatar_mike.png" alt="Mike H Avatar" /></a></div>
									<div class="post-meta"><a href="#">Mike H</a> &bull; 2nd Floor &bull; <a href="#">Classifieds</a></div>
									<div class="post-body">
										<p>Wanted to remind everyone about our annual Labor Day BBQ at the Clubhouse tomorrow at noon! Hot dogs, burgers, veggie burgers, and buns will be provided – please bring something to share. Hope to you see all there!</p>
										<p class="images">
											<img src="http://staging.tenantking.com/uploads/image/file/1/thumb_Screen_Shot_2013-11-11_at_4.27.31_PM.png" data-image="/uploads/image/file/1/Screen_Shot_2013-11-11_at_4.27.31_PM.png" alt="Thumb_screen_shot_2013-11-11_at_4.27.31_pm">
											<img src="http://staging.tenantking.com/uploads/image/file/1/thumb_Screen_Shot_2013-11-11_at_4.27.31_PM.png" data-image="/uploads/image/file/1/Screen_Shot_2013-11-11_at_4.27.31_PM.png" alt="Thumb_screen_shot_2013-11-11_at_4.27.31_pm">
											<img src="http://staging.tenantking.com/uploads/image/file/1/thumb_Screen_Shot_2013-11-11_at_4.27.31_PM.png" data-image="/uploads/image/file/1/Screen_Shot_2013-11-11_at_4.27.31_PM.png" alt="Thumb_screen_shot_2013-11-11_at_4.27.31_pm">
										</p>
									</div>
									<div class="post-comments">
										<ol class="post-comments-list">
											<li>
												<a data-remote="true" class="show-more-comments" href="#">View 3 more comments</a>
											</li>
											<li class="comment">
												<div class="comment-date">Today</div>
												<div class="comment-avatar"><a href="#"><img src="/build/images/avatars/avatar_peter.png" alt="Peter G Avatar" /></a></div>
												<div class="comment-meta"><a href="#">Peter G</a> from <a href="#">2nd Floor</a></div>
												<div class="comment-body"><p>So awesome! I can't wait to faceplant on the big grill. Gonna be epic!</p></div>
											</li>
											<li class="comment">
												<div class="comment-date">Today</div>
												<div class="comment-avatar"><a href="#"><img src="/build/images/avatars/avatar_greg.png" alt="Greg B Avatar" /></a></div>
												<div class="comment-meta"><a href="#">Greg B</a> from <a href="#">32nd Floor</a></div>
												<div class="comment-body"><p>Please make sure everyone brings enough drinks to get this party started. We don’t want to do the same mistake again, as we did last year, when most guest only came with 2 litres of vodka. Looking forward to seeing you guys there!</p></div>
											</li>
										</ol>
									</div>
									<div class="post-actions"><div class="post-actions-container">
										<div class="post-actions-form-container"><form>
											<span class="comment-container">
												<textarea id="comment_body" class="comment-body" rows="1" name="comment[body]" cols="40" style="overflow: hidden; word-wrap: break-word; resize: none; height: 33px;"></textarea>
											</span>
											<button class="comment-submit">Submit</button>
										</form></div>
									</div></div>

								</article>

								<article class="post">

									<h3><a href="#">Hold Up at Gun Point behind SHI @ 2AM Saturday</a></h3>
									<div class="post-status-tag">
										<div class="post-date">Aug 23</div>
										<div class="post-status">Expired</div>
									</div>
									<div class="post-avatar"><a href="#"><img src="/build/images/avatars/avatar_scott.png" alt="Scott D Avatar" /></a></div>
									<div class="post-meta"><a href="#">Scott D</a> &bull; 2nd Floor &bull; <a href="#">General Talk</a></div>
									<div class="post-body"><p>My friend was just having a late picnic around 2AM on early Saturday morning in the alley behind SHI, when two unidentified males robbed him at gun point. After that, my friend fell asleep. </p></div>
									<div class="post-actions"><div class="post-actions-container">
										<div class="post-actions-form-container"><form>
											<span class="comment-container">
												<textarea id="comment_body" class="comment-body" rows="1" name="comment[body]" cols="40" style="overflow: hidden; word-wrap: break-word; resize: none; height: 33px;"></textarea>
											</span>
											<button class="comment-submit">Submit</button>
										</form></div>
									</div></div>
									<div class="post-comments">
										
									</div>
									<div class="post-actions"><div class="post-actions-container">
										<div class="post-actions-form-container"><form>
											<span class="comment-container">
												<textarea id="comment_body" class="comment-body" rows="1" name="comment[body]" cols="40" style="overflow: hidden; word-wrap: break-word; resize: none; height: 33px;"></textarea>
											</span>
											<button class="comment-submit">Submit</button>
										</form></div>
									</div></div>

								</article>

							</div>

							<div class="pagination infinite manual">
								<div class="ajax-loader">
									<img src="/build/images/preloader.gif" alt="Ajax-loader">
								</div>
								<div class="next">
									<a rel="next" data-remote="true" data-manual="true" href="/posts?page=2">Load 20 more posts</a>
								</div>
							</div>

						</div>
					</section>

<?php require_once('sidebar.php'); ?>

