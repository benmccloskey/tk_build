					<aside id="secondary">

						<?php $is_inbox = (($_GET['p'] == 'messages') || ($_GET['p'] == 'messagethread') ? true : false); ?>
						<?php $is_about = (($_GET['p'] == 'about') ? true : false); ?>
						<?php $is_forgot = (($_GET['p'] == 'forgot') || ($_GET['p'] == 'change') ? true : false); ?>


						<h2><?php if($is_inbox): print 'Messages'; elseif($is_forgot): print 'Actions'; else: print 'My Circles'; endif; ?></h2>


						<nav id="secondary-nav"><ul>

							<?php if ($is_inbox): ?>

							<li class="active odd"><a href="/build/?p=messages">Inbox <span>13</span></a></li>
							<li class="even"><a href="/build/?p=messages">Sent</a></li>
							<li class="odd"><a href="/build/?p=messages">Trash</a></li>

							<?php elseif ($is_about): ?>

							<li class="odd active"><a href="#">About</a></li>
							<li class="even"><a href="#">Privacy Policy</a></li>

							<?php elseif ($is_forgot): ?>

							<li class="active odd"><a href="#">Sign in</a></li>
							<li class="even"><a href="#">Sign up</a></li>

							<?php else: ?>

							<li class="odd"><a href="">Long Island City <span>1.8k</span></a></li>
							<li class="active even"><a href="">Gantry Park Landing <span>389</span></a></li>
							<li class="even"><a href="">25th Floor <span>0</span></a></li>

							<?php endif; ?>

						</ul></nav>

						<select id="mobile-menu">

							<?php if ($is_inbox): ?>

							<option value="/build/?p=messages">Inbox</option>
							<option value="/build/?p=messages">Sent</option>
							<option value="/build/?p=messages">Trash</option>

							<?php elseif ($is_about): ?>

							<option value="">About</option>
							<option value="">Privacy Policy</option>

							<?php elseif ($is_forgot): ?>

							<option value="">Sign in</a></option>
							<option value="">Sign up</a></option>

							<?php else: ?>

							<option value="">Long Island City</option>
							<option value="">TF Cornerstone</option>
							<option value="">4615 Center Blvd</option>
							<option value="">25th Floor</option>

							<?php endif; ?>

						</select>

						<div id="mobile-add-post"><a href="#">+ Add post</a></div>

						<?php if (!$is_about && !$is_forgot): ?>

						<div class="cal">
							<h5>Events in the Neighborhood</h5>
							<div class="cal-caption">
								<a href="" class="prev">&lsaquo;</a>
								June 2014
								<a href="" class="next">&rsaquo;</a>
							</div>
							<table class="cal-table">
								<thead class="cal-head">
									<tr>
										<th>Su</th>
										<th>Mo</th>
										<th>Tu</th>
										<th>We</th>
										<th>Th</th>
										<th>Fr</th>
										<th>Sa</th>
									</tr>
								</thead>
								<tbody class="cal-body">
									<tr>
										<td class="cal-off"><div>30</div></td>
										<td><div>1</div></td>
										<td><div>2</div></td>
										<td><div>3</div></td>
										<td><div>4</div></td>
										<td><div><a href="">5</a></div></td>
										<td><div><a href="">6</a></div></td>
									</tr>
									<tr>
										<td><div>7</div></td>
										<td class="cal-selected"><div>8</div></td>
										<td><div>9</div></td>
										<td><div><a href="">10</a></div></td>
										<td><div>11</div></td>
										<td class="cal-check"><div>12</div></td>
										<td><div>13</div></td>
									</tr>
									<tr>
										<td><div><a href="">14</a></div></td>
										<td><div>15</div></td>
										<td><div><a href="">16</a></div></td>
										<td class="cal-check"><div><a href="">17</a></div></td>
										<td><div>18</div></td>
										<td class="cal-today"><div>19</div></td>
										<td><div>20</div></td>
									</tr>
									<tr>
										<td><div>21</div></td>
										<td><div>22</div></td>
										<td><div><a href="">23</a></div></td>
										<td><div><a href="">24</a></div></td>
										<td><div>25</div></td>
										<td><div><a href="">26</a></div></td>
										<td><div>27</div></td>
									</tr>
									<tr>
										<td><div><a href="">28</a></div></td>
										<td><div>29</div></td>
										<td><div>30</div></td>
										<td><div>31</div></td>
										<td class="cal-off"><div><a href="">1</a></div></td>
										<td class="cal-off"><div><a href="">2</a></div></td>
										<td class="cal-off"><div><a href="">3</a></div></td>
									</tr>
								</tbody>
							</table>
							<div id="add-event-link"><a href="">Add an Event</a></div>
						</div>

						<form id="search-box" method="get" action="/posts" accept-charset="UTF-8">
							<h5>Search Posts</h5>
							<div style="margin:0;padding:0;display:inline"><input type="hidden" value="✓" name="utf8"></div>
							<input class="form-text" type="text" name="search" id="search">
							<input class="form-submit" type="submit" value="Search">
						</form>

						<?php endif; ?>

					</aside>