					<section id="primary">
						<div id="primary-inner">

							<section class="forgot-change form">
								
							<h2>Forgot your password?</h2>

							<form novalidate="novalidate" method="post" id="new_user" class="formtastic user" action="/users/password" accept-charset="UTF-8">
								<div style="margin:0;padding:0;display:inline">
									<input type="hidden" value="✓" name="utf8"><input type="hidden" value="nE+jKSB62kI9icivRcZoCVVXv7iMxyqI+ROD8RY3AXY=" name="authenticity_token">
								</div>
								<fieldset class="inputs">
									<ol>
										<li id="user_email_input" class="email input required stringish">
											<label for="user_email" class="label">Email<abbr title="required">*</abbr></label><input type="email" value="" name="user[email]" maxlength="255" id="user_email">
										</li>
									</ol>
								</fieldset>
								<fieldset class="actions">
									<ol>
										<li style="list-style: none">
											<input type="submit" value="Send me reset password instructions" name="commit">
										</li>
									</ol>
								</fieldset>
							</form>

							</section>

						</div>
					</section>

<?php require_once('sidebar.php'); ?>