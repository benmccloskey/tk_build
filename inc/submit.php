					<section id="primary">
						<div id="primary-inner">

							<button id="test-images">Images Switcher</button>

							<section id="post-form" class="show" data-url="/posts/new">

								<h2>Add a post 
									<a style="display:none;" class="show-post-form delete-link add-post-link" href="#">+ ADD POST</a>
									<a class="hide-post-form delete-link hide-post-link" href="#">X Close</a>
								</h2>

							    <div class="toggle-form" style="display: none;">
							        <form method="post" action="/posts" accept-charset="UTF-8">
							            <div style="margin:0;padding:0;display:inline">
							                <input type="hidden" value="&#10003;" name="utf8" /><input type="hidden" value="kRNWaKMZKVdyUaqKr0oXkWHbyD/Fx3l9xM4tY9Du/r4=" name="authenticity_token" />
							            </div>
							            <textarea name="show-post-form" id="show-post-form" class="show-post-form">Click to start entering a new post...</textarea>
							        </form>
							    </div>

							    <div class="post-form">
							        <form novalidate="novalidate" method="post" id="new_post" enctype="multipart/form-data" data-remote="true" class="formtastic post" action="/posts" accept-charset="UTF-8">
							            <div style="margin:0;padding:0;display:inline">
							                <input type="hidden" value="&#10003;" name="utf8" /><input type="hidden" value="kRNWaKMZKVdyUaqKr0oXkWHbyD/Fx3l9xM4tY9Du/r4=" name="authenticity_token" />
							            </div>

							            <fieldset class="inputs">
							                <ol>
							                    <li class="image-errors-container">
							                        <div class="image-errors"></div><input type="hidden" name="category" id="post_category" /> <input type="hidden" value="" name="image_ids" id="image_ids" />
							                    </li>

							                    <li id="post_category_input" class="select input optional">
							                        <label for="post_category_id" class=" label">Category</label><select name="post[category_id]" id="post_category_id" data-placeholder="Select..." class="chzn-select" style="display: none;">
							                            <option value="1">
							                                Classified
							                            </option>

							                            <option value="2">
							                                General Talk
							                            </option>
							                        </select>

							                        <div class="chosen-container chosen-container-single" title="" id="post_category_id_chosen">
							                            <a tabindex="-1" class="chosen-single chosen-default"><span>Select...</span></a>

							                            <div class="chosen-drop">
							                                <div class="chosen-search">
							                                    <input type="text" autocomplete="off" />
							                                </div>

							                                <ul class="chosen-results">
							                                    <li class="active-result" style="" data-option-array-index="1">Classified</li>
							                                    <li class="active-result" style="" data-option-array-index="2">General Talk</li>
							                                </ul>
							                            </div>
							                        </div>
							                    </li>

							                    <li id="post_recipient_input" class="select input required">
							                        <label for="post_recipient" class=" label">Accesible by<abbr title="required">*</abbr></label><select name="post[recipient]" id="post_recipient" class="chzn-select" style="display: none;">
							                            <option value="area">
							                                LIC
							                            </option>

							                            <option value="company">
							                                DOUGLAS ELLIMAN
							                            </option>

							                            <option value="building">
							                                THE GANTRY CONDO
							                            </option>

							                            <option value="floor">
							                                1
							                            </option>
							                        </select>

							                        <div class="chosen-container chosen-container-single" title="" id="post_recipient_chosen">
							                            <a tabindex="-1" class="chosen-single chosen-default"><span>Select an Option</span></a>

							                            <div class="chosen-drop">
							                                <div class="chosen-search">
							                                    <input type="text" autocomplete="off" />
							                                </div>

							                                <ul class="chosen-results">
							                                    <li class="active-result" style="" data-option-array-index="1">Classified</li>
							                                    <li class="active-result" style="" data-option-array-index="2">General Talk</li>
							                                    <li class="active-result" style="" data-option-array-index="1">Some</li>
							                                    <li class="active-result" style="" data-option-array-index="2">More Stuff</li>
							                                </ul>
							                            </div>
							                        </div>
							                    </li>

							                    <li id="post_topic_input" class="string input required stringish"><label for="post_topic" class=" label">Topic<abbr title="required">*</abbr></label><input type="text" name="post[topic]" id="post_topic" /></li>

							                    <li id="post_price_input" class="string input required stringish"><label for="post_price" class=" label">Price<abbr title="required">*</abbr></label><input type="number" name="post[price]" id="post_price" /></li>

							                    <li id="post_body_input" class="text input required with-images">
							                    	<label for="post_body" class=" label">Message<abbr title="required">*</abbr></label>
								                    <textarea rows="20" name="post[body]" id="post_body"></textarea>
													<div class="images-container">
									                    
														<div id="image_13" class="image-container">
															<a href="/build/images/test.png"><img src="/build/images/test.png" alt="Thumb_test"></a>
															<div class="name"></div>
															<div class="actions">
																<a rel="nofollow" data-remote="true" data-method="delete" data-confirm="Are you sure?" href="/images/13">remove</a>
															</div>
														</div>

									                    <div class='image-container' id='upload-container-{%=o.domId%}'>
															<div class='upload'>
																test.png
																<div class='progress'>
																	<div class='bar' style='width: 65%;'></div>
																</div>
															</div>
														</div>
													</div>
							                	</li>
							                </ol>
							            </fieldset>

							            <fieldset class="actions">
							                <div>
							                    <button class="post-submit" type="submit" name="button">Submit</button>
							                    <div class="file_button_container">
							                    	<div id="fake-add-image-button">+</div>
							                        <input type="file" name="image[file]" multiple="multiple" id="new_image" data-url="/images" />
							                    </div>
							                    <a id="cancel-post" href="#">Cancel</a>
							                    <div class="gone-checkbox"><input type="checkbox" name="vehicle" value="Bike">This item is no longer available.</div>
							                </div>
							            </fieldset>

							            <div class="clearer"></div>

							            <div id="images-container"></div>
							        </form>
							    </div>
							    
							</section>

								
							<div id="posts-container">

								<article class="post">

									<h3><a href="#">Missing Gremlin</a></h3>
									<div class="post-status-tag">
										<div class="post-date">Edit | Delete</div>
										<div class="post-status">Sold</div>
									</div>
									<div class="post-avatar"><a href="#"><img src="/build/images/avatars/avatar_patricia.png" alt="Patricia M Avatar" /></a></div>
									<div class="post-meta"><a href="#">Mike D</a> &bull; 4615 Center Blvd &bull; <span class="walking">5 Minutes</span> &bull; <a href="#">Classifieds</a></div>
									<div class="post-body"><p>Our family just got a baby Gremlin about two weeks ago. The kids affectionately named her Gizmo, and played with her in the rooftop pool. Has anyone seen Gizmo?</p></div>
									<div class="post-actions"><div class="post-actions-container">
										<div class="post-actions-form-container"><form>
											<span class="comment-container">
												<textarea id="comment_body" class="comment-body" rows="1" name="comment[body]" cols="40" style="overflow: hidden; word-wrap: break-word; resize: none; height: 33px;"></textarea>
											</span>
											<button class="comment-submit">Submit</button>
										</form></div>
									</div></div>
									<div class="post-comments">
										<ol class="post-comments-list">
											<li class="comment">
												<div class="comment-date">Today</div>
												<div class="comment-avatar"><a href="#"><img src="/build/images/avatars/avatar_peter.png" alt="Peter G Avatar" /></a></div>
												<div class="comment-meta"><a href="#">Peter G</a> from <a href="#">2nd Floor</a></div>
												<div class="comment-body"><p>No, but our pool was swarming with hairy monsters the other day and now the poolboy is missing. Has anyone seen the poolboy?</p></div>
											</li>
										</ol>
										<a data-remote="true" class="show-more-comments" href="#">View 3 more comments</a>
									</div>

								</article>


								<article class="post">

									<h3><a href="#">Reminder: Labor Day BBQ tomorrow!</a></h3>
									<div class="post-status-tag">
										<div class="post-date">Aug 31</div>
										<div class="post-status post-price">$1,345,789.00</div>
									</div>
									<div class="post-avatar"><a href="#"><img src="/build/images/avatars/avatar_mike.png" alt="Mike H Avatar" /></a></div>
									<div class="post-meta"><a href="#">Mike H</a> &bull; 2nd Floor &bull; <a href="#">Classifieds</a></div>
									<div class="post-body">
										<p>Wanted to remind everyone about our annual Labor Day BBQ at the Clubhouse tomorrow at noon! Hot dogs, burgers, veggie burgers, and buns will be provided – please bring something to share. Hope to you see all there!</p>
										<p class="images">
											<img src="http://staging.tenantking.com/uploads/image/file/1/thumb_Screen_Shot_2013-11-11_at_4.27.31_PM.png" data-image="/uploads/image/file/1/Screen_Shot_2013-11-11_at_4.27.31_PM.png" alt="Thumb_screen_shot_2013-11-11_at_4.27.31_pm">
											<img src="http://staging.tenantking.com/uploads/image/file/1/thumb_Screen_Shot_2013-11-11_at_4.27.31_PM.png" data-image="/uploads/image/file/1/Screen_Shot_2013-11-11_at_4.27.31_PM.png" alt="Thumb_screen_shot_2013-11-11_at_4.27.31_pm">
											<img src="http://staging.tenantking.com/uploads/image/file/1/thumb_Screen_Shot_2013-11-11_at_4.27.31_PM.png" data-image="/uploads/image/file/1/Screen_Shot_2013-11-11_at_4.27.31_PM.png" alt="Thumb_screen_shot_2013-11-11_at_4.27.31_pm">
										</p>
									</div>
									<div class="post-actions"><div class="post-actions-container">
										<div class="post-actions-form-container"><form>
											<span class="comment-container">
												<textarea id="comment_body" class="comment-body" rows="1" name="comment[body]" cols="40" style="overflow: hidden; word-wrap: break-word; resize: none; height: 33px;"></textarea>
											</span>
											<button class="comment-submit">Submit</button>
										</form></div>
									</div></div>
									<div class="post-comments">
										<ol class="post-comments-list">
											<li class="comment">
												<div class="comment-date">Today</div>
												<div class="comment-avatar"><a href="#"><img src="/build/images/avatars/avatar_peter.png" alt="Peter G Avatar" /></a></div>
												<div class="comment-meta"><a href="#">Peter G</a> from <a href="#">2nd Floor</a></div>
												<div class="comment-body"><p>So awesome! I can't wait to faceplant on the big grill. Gonna be epic!</p></div>
											</li>
											<li class="comment">
												<div class="comment-date">Today</div>
												<div class="comment-avatar"><a href="#"><img src="/build/images/avatars/avatar_greg.png" alt="Greg B Avatar" /></a></div>
												<div class="comment-meta"><a href="#">Greg B</a> from <a href="#">32nd Floor</a></div>
												<div class="comment-body"><p>Please make sure everyone brings enough drinks to get this party started. We don’t want to do the same mistake again, as we did last year, when most guest only came with 2 litres of vodka. Looking forward to seeing you guys there!</p></div>
											</li>
										</ol>
									</div>

								</article>

								<article class="post">

									<h3><a href="#">Hold Up at Gun Point behind SHI @ 2AM Saturday</a></h3>
									<div class="post-status-tag">
										<div class="post-date">Aug 23</div>
										<div class="post-status">Expired</div>
									</div>
									<div class="post-avatar"><a href="#"><img src="/build/images/avatars/avatar_scott.png" alt="Scott D Avatar" /></a></div>
									<div class="post-meta"><a href="#">Scott D</a> &bull; 2nd Floor &bull; <a href="#">General Talk</a></div>
									<div class="post-body"><p>My friend was just having a late picnic around 2AM on early Saturday morning in the alley behind SHI, when two unidentified males robbed him at gun point. After that, my friend fell asleep. </p></div>
									<div class="post-actions"><div class="post-actions-container">
										<div class="post-actions-form-container"><form>
											<span class="comment-container">
												<textarea id="comment_body" class="comment-body" rows="1" name="comment[body]" cols="40" style="overflow: hidden; word-wrap: break-word; resize: none; height: 33px;"></textarea>
											</span>
											<button class="comment-submit">Submit</button>
										</form></div>
									</div></div>
									<div class="post-comments">
										
									</div>

								</article>

							</div>


						</div>
					</section>

<?php require_once('sidebar.php'); ?>

