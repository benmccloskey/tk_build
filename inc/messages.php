					<section id="primary">
						<div id="primary-inner">

							<h1 id="page-title" class="page-title-inbox">Inbox</h1>

							<ol id="inbox-list">
								<li class="odd">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_patricia.png" alt="Patricia M Avatar" /></div>
									<div class="inbox-subject">
										<a class="unread" href="/build/?p=messagethread">RE: Borrow a ladder?</a>
										<div class="inbox-from">Patricia M.</div>
									</div>
									<div class="inbox-time">15min ago</div>
								</li>
								<li class="even">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_peter.png" alt="Peter G Avatar" /></div>
									<div class="inbox-subject">
										<a class="unread" href="/build/?p=messagethread">RE: Neoghborhood garage sale?</a>
										<div class="inbox-from">Peter G.</div>
									</div>
									<div class="inbox-time">2H ago</div>
								</li>
								<li class="odd">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_mike.png" alt="Mike H Avatar" /></div>
									<div class="inbox-subject">
										<a class="read" href="/build/?p=messagethread">Free Trundle Bed and Mattress</a>
										<div class="inbox-from">Mike H.</div>
									</div>
									<div class="inbox-time">Sep 29</div>
								</li>
								<li class="even">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_peter.png" alt="Peter G Avatar" /></div>
									<div class="inbox-subject">
										<a class="read" href="/build/?p=messagethread">RE: Neoghbborhood garage sale?</a>
										<div class="inbox-from">Peter G.</div>
									</div>
									<div class="inbox-time">Sep 28</div>
								</li>
								<li class="odd">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_greg.png" alt="Greg B Avatar" /></div>
									<div class="inbox-subject">
										<a class="read" href="/build/?p=messagethread">4 Free Tickets to the college basketball game / Saturday Feb 19th</a>
										<div class="inbox-from">Greg B.</div>
									</div>
									<div class="inbox-time">Sep 28</div>
								</li>
								<li class="even">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_peter.png" alt="Peter G Avatar" /></div>
									<div class="inbox-subject">
										<a class="unread" href="/build/?p=messagethread">RE: MacBook Pro 15", Top of Line bought Oct '09, $250 OBO</a>
										<div class="inbox-from">Peter G.</div>
									</div>
									<div class="inbox-time">Sep 28</div>
								</li>
								<li class="odd">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_patricia.png" alt="Patricia M Avatar" /></div>
									<div class="inbox-subject">
										<a class="read" href="/build/?p=messagethread">RE: Desk for $30</a>
										<div class="inbox-from">Patricia M.</div>
									</div>
									<div class="inbox-time">Sep 27</div>
								</li>
								<li class="even">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_mike.png" alt="Mike H Avatar" /></div>
									<div class="inbox-subject">
										<a class="unread" href="/build/?p=messagethread">Lots of neighbors at the Memorial Day BBQ!</a>
										<div class="inbox-from">Mike H.</div>
									</div>
									<div class="inbox-time">Sep 25</div>
								</li>
								<li class="odd">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_greg.png" alt="Greg B Avatar" /></div>
									<div class="inbox-subject">
										<a class="read" href="/build/?p=messagethread">RE: Does anyone know of a good local seamstress or tailor?</a>
										<div class="inbox-from">Greg B.</div>
									</div>
									<div class="inbox-time">Sep 23</div>
								</li>
								<li class="even">
									<div class="inbox-avatar"><img src="/build/images/avatars/avatar_peter.png" alt="Peter G Avatar" /></div>
									<div class="inbox-subject">
										<a class="read" href="/build/?p=messagethread">Umbrellas for sale</a>
										<div class="inbox-from">Peter G.</div>
									</div>
									<div class="inbox-time">Sep 23</div>
								</li>
							</ol>
							
							<div id="pager">
								1-20 of 365
								<ul>
									<li id="pager-previous"><a href="#">Previous</a></li>
									<li id="pager-next"><a href="#">Next</a></li>
								</ul>
							</div>

						</div>
					</section>

<?php require_once('sidebar.php'); ?>