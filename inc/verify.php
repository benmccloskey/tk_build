			<div class="inner">
				<div id="verify-container">
					
					<?php
					$is_login = (isset($_GET['p']) ? true : false); // check if this is login page
					$is_login ? $submit_text = 'Log In' : $submit_text = 'Activate My Account';
					$is_login ? $verify_title = 'Log In To Your Tenant King Account' : $verify_title = 'Private Network For Your Building.';
					$is_login ? $form_links = 'Forgot your password? <a href="#">Click here</a>' : $form_links = '<ol><li>Already a member? <a href="/build?p=login">Sign in</a></li><li class="request-invitation-link"><a href="#request-invitation">Request an Invitation</a></li></ol>';
					?>

					<h1>Choose a verification method.</h1>

					<div id="verify-invite-link">Already have an invitation code? <a href="#">Click Here</a></div>

					<div id="verify-main">
						
						<div id="verify-choose">Or choose one of the following options:</div>

						<div class="verify-method">
							<h2>Verify by credit card.</h2>
							<p>
								The fastest option to verify your address. We will only check the billing address and <strong>no payment is required</strong>. The billing address on the card must be: <br><strong>4545 Center Blvd, Apt 1001, Long Island City</strong>.
							</p>
							<button class="verify-expand">Click Here</button>
							<div class="verify-form">
								<form method="post" action="/build/?p=verify&success=cc" id="verify-cc">
									<fieldset class="inputs">
										<ol>
											<li>
												<label for="fname">First Name</label>
												<input class="form-text" type="text" name="fname">
											</li>
											<li>
												<label for="lname">Last Name</label>
												<input class="form-text" type="text" name="lname">
											</li>
											<li>
												<label for="ccnum">Card Number</label>
												<input class="form-text" type="text" name="ccnum">
											</li>
											<li class="half">
												<label for="ccexp">Exp (mm/yy)</label>
												<input class="form-text" type="text" name="ccexp">
											</li>
											<li class="half last">
												<label for="ccv">CCV</label>
												<input class="form-text" type="text" name="ccv">
											</li>
										</ol>
									</fieldset>
									<fieldset class="actions">
										<button type="submit">Verify my address</button>
									</fieldset>
								</form>
							</div>
						</div>

						<div class="verify-method">
							<h2>Verify by mail.</h2>
							<p>
								Tenant King will send you a letter containing a unique invitation code. You can then use this invitation code to verify your address. The letter can take <strong>5-7 days</strong> to reach your mailbox.
							</p>
							<button class="verify-expand">Click Here</button>
							<div class="verify-form">
								<form method="post" action="/build/?p=verify&success=mail" id="verify-mail">
									<fieldset class="inputs">
										<ol>
											<li>
												<label for="fname">First Name</label>
												<input class="form-text" type="text" name="fname">
											</li>
											<li>
												<label for="lname">Last Name</label>
												<input class="form-text" type="text" name="lname">
											</li>
										</ol>
									</fieldset>
									<fieldset class="actions">
										<button type="submit">Send me a letter</button>
									</fieldset>
								</form>
							</div>
						</div>

						<div class="verify-method last">
							<h2>Upload a utility bill.</h2>
							<p>
								Upload a utility bill containing your address and unit number. A member of the Tenant King team will inspect the document and you will receive an e-mail confirming your account activation. <strong>Usually takes 2 days to process.</strong>
							</p>
							<button class="verify-expand">Click Here</button>
							<div class="verify-form">
								<form method="post" action="/build/?p=verify&success=bill" id="verify-bill">
									<fieldset class="inputs">
										<ol>
											<li>
												<label for="fname">First Name</label>
												<input class="form-text" type="text" name="fname">
											</li>
											<li>
												<label for="lname">Last Name</label>
												<input class="form-text" type="text" name="lname">
											</li>
											<li>
												<label for="bill">Choose a file to upload</label>
												<div id="bill-upload-facade">
													<div id="bill-upload-facade-filename"></div>
													<button id="bill-upload-facade-browse">Browse</button>
													<input class="form-upload" type="file" name="bill">
												</div>
												
											</li>
										</ol>
									</fieldset>
									<fieldset class="actions">
										<button type="submit">Upload the utility bill</button>
									</fieldset>
								</form>
							</div>
						</div>






						<div class="clear">&nbsp;</div>

					</div>

					<div class="clear">&nbsp;</div>

				</div>
			</div>










