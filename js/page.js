$(document).ready(function(){

	var isTouch = (('ontouchstart' in window) || (navigator.msMaxTouchPoints > 0));
	if(isTouch){
		$('html').addClass('touch');
	}else{
		$('html').addClass('no-touch');
	}


	$('#test-images').click(function(toggleImages) {
		$('#post_body_input, #message_body_input').toggleClass('with-images');
		$('.images-container').toggle();
	});

	$('.post-form form fieldset.inputs ol li.select .chosen-container a.chosen-single').click(function() {
		$(this).siblings('.chosen-drop').slideToggle('fast', function() {
			$('#overlay').toggleClass('on');
		});
		$(this).toggleClass('overlay-over');
		});
	$('.post-form form fieldset.inputs ol li.select .chosen-container .chosen-drop li').click(function() {
		$(this).siblings('li').removeClass('selected');
		$(this).addClass('selected');
		var selectedText = $(this).html();
		if(selectedText == 'Classified'){
			$('.post-form form').addClass('classified');
		}else{
			$('.post-form form').removeClass('classified');
		}
		$(this).closest('.chosen-drop').slideToggle('fast', function() {
			$('#overlay').toggleClass('on');
			$(this).siblings('a.chosen-single').toggleClass('overlay-over');
			$(this).siblings('a.chosen-single').html('<span>'+selectedText+'</span>');
		});
	});

	// link alert
	$('nav#main-nav ul li span.nav-alert').click(function() {
		window.location.href = $(this).siblings("a").attr("href");
	});

	// link rows
	$('#topics-table tbody tr,#inbox-list li').click(function() {
		window.location.href = $(this).find("a").attr("href");
	});
	
	$('#welcome-promo-2 .welcome-list-link a').click(function(expandblist) {
		expandblist.preventDefault();
		$('#welcome-building-list').slideToggle('fast');
	});

	$('.pagination.infinite .next').click(function(loadmore) {
		loadmore.preventDefault();
		$('.pagination.infinite .ajax-loader, .pagination.infinite .next').toggle();
	});
	
	$('select#mobile-menu').change(function(){
	   if(navigator.userAgent.match(/(iPhone|iPod|iPad)/i)){
	   		$('#secondary').hide();
	   	}
	   var url = $(this).val();
	   window.location = url;
	});

	$("#verify-main .verify-method .verify-form input.form-upload").change(function (){
		var billUploadFileName = $(this).val().replace(/C:\\fakepath\\/i, '');
		$("#bill-upload-facade-filename").html(billUploadFileName);
    });

    $('button.verify-expand').click(function(showform) {
		showform.preventDefault();
		
		//fade the box
		$(this).parent().removeClass('faded', 200);
		//open the box
		$(this).parent().toggleClass('open');
		//open the form
		$(this).siblings('.verify-form').slideToggle(200);
		//hide the button
		$(this).toggleClass('hidden', 100);

		//close the other stuff
		$('button.verify-expand').not(this).removeClass('hidden', 100);
		$('.verify-form').not($(this).siblings('.verify-form')).removeClass('open', 200);
		$('.verify-form').not($(this).siblings('.verify-form')).slideUp(200);
		$('.verify-method').not($(this).parent()).addClass('faded', 200);
		$('.verify-method').not($(this).parent()).removeClass('open', 200);
	});

    // match post heading right margin to width of price tag

    if($('.post h3').length){
    	$('.post h3').each(function() {
    		var newWidth = $(this).siblings('.post-status-tag').width() + 10;
    		$(this).css('margin-right',newWidth);
    	});
    }

	$('textarea').autosize();

	$('textarea#comment_body').bind('keyup',function() {
	    var textareaWidth = $(this).width();
	    if(textareaWidth == 572){
	    	var threshold = 65;
	    }if(textareaWidth < 572 && textareaWidth > 379){
	    	var threshold = 52;
	    }if(textareaWidth <= 379){
	    	var threshold = 26;
	    }
	    var len = $(this).val().length;
	    if (len > threshold){
	    	$(this).parent().siblings('button.comment-submit').addClass('under', 150);
	    }else{
	    	$(this).parent().siblings('button.comment-submit').removeClass('under', 150);
	    }
	});

	if(navigator.userAgent.match(/(iPhone|iPod|iPad)/i)){
		$('select#mobile-menu').focus(function() {
			$('#secondary').css("opacity", "0");
		});
	
		$('select#mobile-menu').blur(function() {
			$('#secondary').css("opacity", "1");
		});
	}
	$(".request-invitation-link a").click(function (event) {
	    event.preventDefault();
	    //calculate destination place
	    var dest = 0;
	    if ($(this.hash).offset().top > $(document).height() - $(window).height()) {
	        dest = $(document).height() - $(window).height();
	    } else {
	        dest = $(this.hash).offset().top;
	    }
	    //go to destination
	    $('html,body').animate({
	        scrollTop: dest
	    }, 500, 'swing');
	    $('input#building-address').focus();
	});
	
});