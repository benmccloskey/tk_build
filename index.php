<?php require_once('inc/header.php'); // include header ?>

<?php

// determine which page to get
switch ((isset($_GET['p']) ? $_GET['p'] : '')) {
	case 'wall':
		$content = 'inc/wall.php';
		break;

	case 'submit':
		$content = 'inc/submit.php';
		break;
	
	case 'messages':
		$content = 'inc/messages.php';
		break;

	case 'messagethread':
		$content = 'inc/messagethread.php';
		break;

	case 'notifications':
		$content = 'inc/notifications.php';
		break;
	
	case 'topics':
		$content = 'inc/topics.php';
		break;
	
	case 'classifieds':
		$content = 'inc/topics.php';
		break;

	case 'settings':
		$content = 'inc/settings.php';
		break;

	case 'about':
		$content = 'inc/about.php';
		break;

	case 'login':
		$content = 'inc/front.php';
		break;

	case 'forgot':
		$content = 'inc/forgot.php';
		break;

	case 'change':
		$content = 'inc/change.php';
		break;

	case 'verify':
		$content = 'inc/verify.php';
		break;

	case 'activate':
		$content = 'inc/activate.php';
		break;
	
	default:
		$content = 'inc/front.php';
		break;
}
// include appropriate file
require_once($content);

?>

<?php require_once('inc/footer.php'); // include footer ?>